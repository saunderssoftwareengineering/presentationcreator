package com.saunders.presentation_creator.data;

public class SalesSummaryException extends Exception {

	private static final long serialVersionUID = -1135433797466373407L;

	public SalesSummaryException()
	{
		super();
	}

	public SalesSummaryException(String message)
	{
		super(message);
	}
	
	public SalesSummaryException(Throwable t)
	{
		super(t);
	}
	
	public SalesSummaryException(String message, Throwable t)
	{
		super(message, t);
	}


}
