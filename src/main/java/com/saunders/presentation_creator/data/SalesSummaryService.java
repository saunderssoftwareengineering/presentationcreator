package com.saunders.presentation_creator.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.saunders.presentation_creator.model.SalesSummary;

public class SalesSummaryService {

	public static List<SalesSummary> getSalesSummaries(File spreadsheet) throws SalesSummaryException {
		List<SalesSummary> salesSummaries = new ArrayList<SalesSummary>();

		FileInputStream inputStream = null;
		XSSFWorkbook workbook = null;

		try {
			inputStream = new FileInputStream(spreadsheet);
			workbook = new XSSFWorkbook(inputStream);
			XSSFSheet sheet = workbook.getSheetAt(0);
			Iterator<Row> rowIterator = sheet.iterator();

			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				salesSummaries.add(getSalesSummaryFromRow(row));
			}
		} catch (Throwable t) {
			t.printStackTrace();
			throw new SalesSummaryException(t);
		} finally {
			try {
				inputStream.close();
				workbook.close();
			} catch (IOException e) {
			}
		}
		return salesSummaries;
	}
	
	private static SalesSummary getSalesSummaryFromRow(Row row)
	{
		SalesSummary salesSummary = new SalesSummary();
		salesSummary.setSalesPerson(row.getCell(0).getStringCellValue());
		Double month1 = row.getCell(1).getNumericCellValue();
		Double month2 = row.getCell(2).getNumericCellValue();
		Double month3 = row.getCell(3).getNumericCellValue();
		Double month4 = row.getCell(4).getNumericCellValue();
		List<Double> sales = new ArrayList<Double>();
		sales.add(month1);
		sales.add(month2);
		sales.add(month3);
		sales.add(month4);
		salesSummary.setSales(sales);
		salesSummary.setTotal(month1 + month2 + month3 + month4);
		salesSummary.setAverage(salesSummary.getTotal() / 4);
		return salesSummary;
	}
}
