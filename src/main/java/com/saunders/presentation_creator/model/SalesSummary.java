package com.saunders.presentation_creator.model;

import java.util.List;

public class SalesSummary {
	
	private String salesPerson;
	private List<Double> sales;
	private Double total;
	private Double average;
	
	public String getSalesPerson() {
		return salesPerson;
	}
	public void setSalesPerson(String salesPerson) {
		this.salesPerson = salesPerson;
	}
	public List<Double> getSales() {
		return sales;
	}
	public void setSales(List<Double> sales) {
		this.sales = sales;
	}
	public Double getTotal() {
		return total;
	}
	public void setTotal(Double total) {
		this.total = total;
	}
	public Double getAverage() {
		return average;
	}
	public void setAverage(Double average) {
		this.average = average;
	}
	@Override
	public String toString() {
		return "SalesSummary [salesPerson=" + salesPerson + ", sales=" + sales + ", total=" + total + ", average="
				+ average + "]";
	}
}
