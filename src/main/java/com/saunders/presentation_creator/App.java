package com.saunders.presentation_creator;

import java.io.File;
import java.util.List;

import com.saunders.presentation_creator.creator.SalesSummariesPresentationCreator;
import com.saunders.presentation_creator.data.SalesSummaryService;
import com.saunders.presentation_creator.model.SalesSummary;

public class App {
	public static void main(String[] args) {

		try {
			File spreadsheet = new File("files/sales.xlsx");
			List<SalesSummary> salesSummaries = SalesSummaryService.getSalesSummaries(spreadsheet);
			File presentation = new File("files/sales.pptx");
			SalesSummariesPresentationCreator.createPresentation(salesSummaries, presentation);
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
}
