package com.saunders.presentation_creator.creator;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.NumberFormat;
import java.time.Month;
import java.util.List;

import org.apache.poi.xslf.usermodel.SlideLayout;
import org.apache.poi.xslf.usermodel.XMLSlideShow;
import org.apache.poi.xslf.usermodel.XSLFSlide;
import org.apache.poi.xslf.usermodel.XSLFSlideLayout;
import org.apache.poi.xslf.usermodel.XSLFSlideMaster;
import org.apache.poi.xslf.usermodel.XSLFTextShape;

import com.saunders.presentation_creator.model.SalesSummary;

public class SalesSummariesPresentationCreator {

	public static void createPresentation(List<SalesSummary> salesSummaries, File outputFile) throws PresentationCreatorException {

		XMLSlideShow ppt = null;
		FileOutputStream out = null;
		
		try {
			ppt = new XMLSlideShow();
			XSLFSlideMaster defaultMaster = ppt.getSlideMasters().get(0);
			XSLFSlideLayout titleBodyLayout = defaultMaster.getLayout(SlideLayout.TITLE_AND_CONTENT);

			for (SalesSummary salesSummary : salesSummaries) {
				XSLFSlide slide = ppt.createSlide(titleBodyLayout);
				XSLFTextShape title = slide.getPlaceholder(0);
				title.setText(salesSummary.getSalesPerson() + "'s Sales");
				XSLFTextShape body = slide.getPlaceholder(1);
				body.clearText();
				body.addNewTextParagraph().addNewTextRun().setText(getPresentationBodyFromSalesSummary(salesSummary));
			}
			
			out = new FileOutputStream(outputFile);
			ppt.write(out);
		} catch (Throwable t) {
			t.printStackTrace();
			throw new PresentationCreatorException(t);
		}
		finally
		{
			try {
				ppt.close();
				out.close();
			} catch (IOException e) {
			}
		}
	}

	private static String getPresentationBodyFromSalesSummary(SalesSummary salesSummary) {
		StringBuilder buffer = new StringBuilder();
		NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance();
		
		int index = 1;
		for(Double salesTotalForMonth : salesSummary.getSales())
		{
			buffer.append(Month.of(index++) + " : " + currencyFormatter.format(salesTotalForMonth) + "\n");
		}	
		
		buffer.append("Monthly Average : " + currencyFormatter.format(salesSummary.getAverage()) + "\n");
		buffer.append("Total Sales : " + currencyFormatter.format(salesSummary.getTotal()));

		return buffer.toString();
	}

}
