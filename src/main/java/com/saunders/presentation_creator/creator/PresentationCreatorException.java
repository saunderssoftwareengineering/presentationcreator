package com.saunders.presentation_creator.creator;

public class PresentationCreatorException extends Exception {

	private static final long serialVersionUID = 2864913136261083702L;

	public PresentationCreatorException() {
		super();
	}

	public PresentationCreatorException(String message) {
		super(message);
	}

	public PresentationCreatorException(Throwable t) {
		super(t);
	}

	public PresentationCreatorException(String message, Throwable t) {
		super(message, t);
	}

}
